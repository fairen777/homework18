#include <iostream>
#include <stack>


using namespace std;

template <typename S> //Inplementation of solution using tamplates
class sampleStack
{
private:
	int i_ = 0;
	int size_ = 0;
	S* p_ = new S[size_]();
public:
	sampleStack  (int size) : size_(size)
	{}
	void push(S e)
	{
		p_[i_] = e;
		p_[i_++];
	}
	void top()
	{
		if (i_ <= 0)
		{
			cout << p_[i_] << endl;
		}
		else
		{
			cout << p_[i_ - 1] << endl;
		}
	}
	void pop()
	{
		if (i_ <= 0)
		{
			cout << "Stack is empty. Use comand push() \n";
		}
		else
		{
			
			p_[i_--];
			p_[i_] = '\0';
		}
	}
	void showAll()
	{
		for (int i = 0; i < size_; i++)
		{
			cout << p_[i];
		}
	}
	void dellStack()
	{
		delete[] p_;
	}
};

class DynStack //Implementation of solution with dynamic memory allocation for array
{
private:
	int n_=0;
	int* p_ = new int[n_]();
	int i_ = 0;

public:
	DynStack(int n) : n_(n)
	{}
	void push(int e)
	{
		p_[i_] = e;
		p_[i_++];
		
	}
	void pop()
	{
		if (i_ <= 0)
		{
			cout << "Stack is empty. Use comand push() \n";
		}
		else
		{
			p_[i_--];
			p_[i_] = '\0';
		}
	}
	void top()
	{
		if (i_ <= 0)
		{
			cout << p_[i_] << endl;
		}
		else
		{
			cout << p_[i_-1] << endl;
		}
	}
	void showAll()
	{
		for (int i = 0; i < n_; i++)
		{
			cout << p_[i] << '/';
		}
	}
	void dellStack()
	{
		delete[] p_;
	}
};

class StaticStack10 //Implementation with static array solution
{
private:
	int i_= 0;
	int a[10]= { 0 };

public:
	StaticStack10()
	{}
	void push(int i)
	{
		a[i_] = i;
		a[i_++];
	}
	void top()
	{
		cout << a[i_-1] << endl;
	}
	void pop()
	{
		a[i_--];
	}
	void showAll()
	{
		for (int i = 0; i < 10; i++)
		{
			cout << a[i] << '/';
		}
	}
};

int main()
{
	sampleStack<float> stack5(4);
	stack5.push(23.5);
	stack5.push(11.34);
	stack5.pop();
	stack5.showAll();
	stack5.dellStack();

	cout << endl;

	sampleStack<char> stack4(10);
	stack4.push('H');
	stack4.push('e');
	stack4.push('l');
	stack4.push('l');
	stack4.push('o');
	stack4.top();
	stack4.push('y');
	stack4.pop();
	stack4.top();
	stack4.showAll();
	stack4.dellStack();

	cout << "\n---------------------\n";

	DynStack stack3(20);
	stack3.top();
	stack3.pop();
	stack3.push(5);
	stack3.top();
	stack3.push(8);
	stack3.top();
	stack3.push(13);
	stack3.top();
	stack3.showAll();
	stack3.dellStack();

	cout << "\n--------------------\n";

	StaticStack10 stack2;
	stack2.top();
	stack2.push(2);
	stack2.top();
	stack2.push(44);
	stack2.push(3);
	stack2.top();
	stack2.pop();
	stack2.top();
	stack2.push(22);
	stack2.showAll(); 

	cout << "\n-------------------\n";

	stack <int> stack1;
	for (int i = 0; i < 6; ++i)
	{
		stack1.push(i);
		cout << stack1.top() << '\n';
	}

	cout << '\n';
	stack1.pop();
	
	cout << stack1.top() << "\n";
	return 0;
}